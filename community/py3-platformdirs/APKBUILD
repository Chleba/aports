# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-platformdirs
pkgver=4.2.1
pkgrel=0
pkgdesc="Module for determining appropriate platform-specific dirs"
url="https://github.com/platformdirs/platformdirs"
arch="noarch"
license="MIT"
makedepends="py3-gpep517 py3-hatchling py3-hatch-vcs"
checkdepends="py3-appdirs py3-pytest py3-pytest-mock"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/p/platformdirs/platformdirs-$pkgver.tar.gz"
builddir="$srcdir/platformdirs-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
c6c671022a74ba35610469f08e9a275daa19f0362a98208096fab147a77abfc96c12a0312fdf65668b3e457e0c06d938f141efec1217ef1a7598a77f65786130  platformdirs-4.2.1.tar.gz
"

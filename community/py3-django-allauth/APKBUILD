# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-django-allauth
_pyname="django-allauth"
pkgver=0.62.1
pkgrel=0
arch="noarch"
pkgdesc="Integrated set of Django applications addressing SSO"
url="https://pypi.python.org/project/django-allauth"
license="MIT"
# missing opt depends: python3-openid
depends="
	py3-python3-saml
	py3-qrcode
	py3-django
	py3-openid
	py3-requests
	py3-requests-oauthlib
	py3-jwt
	py3-cryptography
	"
makedepends="
	py3-setuptools
	py3-gpep517
	py3-wheel
	"
checkdepends="
	py3-pytest
	py3-pytest-django
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/pennersr/$_pyname/archive/$pkgver.tar.gz"
builddir="$srcdir"/$_pyname-$pkgver
subpackages="$pkgname-pyc"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -v
}

package() {
	python3 -m installer --destdir="$pkgdir" .dist/*.whl
}

sha512sums="
43aeb4e81382bf8f8a616730db365f08c5c73e607606cb38107693d5920504136e3cc66c8515d15343f74ffb7d374d1f6172deba7bc7b3407fa29343a20d95c7  py3-django-allauth-0.62.1.tar.gz
"
